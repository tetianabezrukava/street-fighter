import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    let firstFighterHealth = firstFighter.health;
    let firstFighterBlock = false;
    let secondFighterHealth = secondFighter.health;
    let secondFighterBlock = false;
    let firstFighterLastCriticalHit = 0;
    let secondFighterLastCriticalHit = 0;
    
  
    document.addEventListener('keyup',
      (e) => runAttack(e, controls.PlayerOneAttack, firstFighter));
    document.addEventListener('keyup',
      (e) => runAttack(e, controls.PlayerTwoAttack, secondFighter));
    document.addEventListener('keydown',
      (e) => toggleBlock(e, controls.PlayerOneBlock, firstFighter, true));
    document.addEventListener('keyup',
      (e) => toggleBlock(e, controls.PlayerOneBlock, firstFighter, false));
    document.addEventListener('keydown',
      (e) => toggleBlock(e, controls.PlayerTwoBlock, secondFighter, true));
    document.addEventListener('keyup',
      (e) => toggleBlock(e, controls.PlayerTwoBlock, secondFighter, false));
    
    runCriticalHit(controls.PlayerOneCriticalHitCombination, firstFighter);
    runCriticalHit(controls.PlayerTwoCriticalHitCombination, secondFighter);

  
    function toggleBlock(e, blockKey, fighter, flag) {
      if (e.code === blockKey) {
        if (fighter === firstFighter) {
          firstFighterBlock = flag;
        } else {
          secondFighterBlock = flag;
        }
      }
    }
    
    function runAttack(e, attackKey, attacker) {
      if ((secondFighterHealth <= 0) || (firstFighterHealth <= 0)) { return }
      
      const attackerBlock = attacker === firstFighter ? firstFighterBlock : secondFighterBlock;
      const defenderBlock = attacker === firstFighter ? secondFighterBlock : firstFighterBlock;
      const defender = attacker === firstFighter ? secondFighter : firstFighter;
      
      if (e.code === attackKey && !attackerBlock && !defenderBlock) {
        const damage = getDamage(attacker, defender);
        if(attacker === firstFighter) {
          secondFighterHealth -= damage;
        } else {
          firstFighterHealth -= damage;
        }
      }
      
      changeIndicator(defender);
      
      checkWinner();
    }
  
    function runCriticalHit(codes, attacker) {
      let pressed = new Set();
  
      document.addEventListener('keydown', function(event) {
        pressed.add(event.code);
    
        for (let code of codes) {
          if (!pressed.has(code)) {
            return;
          }
        }
    
        pressed.clear();
        
        let lastCriticalHit = attacker === firstFighter ? firstFighterLastCriticalHit : secondFighterLastCriticalHit;
        if (new Date() - lastCriticalHit > 10000) {
          console.log(attacker.name, ' critical hit');
          if (attacker === firstFighter) {
            firstFighterLastCriticalHit = new Date();
            secondFighterHealth -= attacker.attack * 2;
          } else {
            secondFighterLastCriticalHit = new Date();
            firstFighterHealth -= attacker.attack * 2;
          }
        }
      });
  
      document.addEventListener('keyup', function(event) {
        pressed.delete(event.code);
      });
    }
    
    function changeIndicator(defender) {
      const defenderCurrentHealth = defender === firstFighter ? firstFighterHealth : secondFighterHealth;
      const indicatorId = defender === firstFighter ? 'left-fighter-indicator' : 'right-fighter-indicator';
      const healthIndicator = document.getElementById(indicatorId);
      healthIndicator.style.width = `${Math.floor(100 / defender.health * defenderCurrentHealth)}%`
    }
    
    function checkWinner() {
      if (firstFighterHealth<= 0 || secondFighterHealth <= 0) {
        if (firstFighterHealth <= 0) { resolve(secondFighter) }
        if (secondFighterHealth <= 0) { resolve(firstFighter) }
      }
    }
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return blockPower >= hitPower ? 0 : hitPower - blockPower;
}

export function getHitPower(fighter) {
  return fighter.attack * random(1, 2);
}

export function getBlockPower(fighter) {
  return fighter.defense * random(1, 2);
}

function random(min, max) {
  return min + Math.random() * (max - min);
}
