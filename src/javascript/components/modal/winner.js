import { showModal } from './modal';
import App from '../../app';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const title = `The Game Is Over`;
  const bodyElement = createElement({tagName: 'div', className: 'modal-body'});
  bodyElement.innerHTML = `${fighter.name} has won`
  const onClose = () => {
    const rootElement = document.getElementById('root');
    rootElement.innerHTML = '';
    new App();
  };
  showModal({title, bodyElement, onClose});
}
