import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  
  // todo: show fighter info (image, name, health, etc.)
  
  const {name, health, attack, defense, source} = fighter;
  fighterElement.append(createFighterImage({name, source}))
  fighterElement.innerHTML += (
    `
    <div class="fighter-preview__name">${name}</div>
    <div class="fighter-preview__health">Health: <span>${health}</span></div>
    <div class="fighter-preview__attack">Attack: <span>${attack}</span></div>
    <div class="fighter-preview__defense">Defense: <span>${defense}</span></div>
    `
  )
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
